# build image
docker build -t lirimia/tasmoadmin .

# test image !
docker tag lirimia/tasmoadmin lirimia/tasmoadmin:v1.7.0

# push image
docker login
docker push lirimia/tasmoadmin
docker push lirimia/tasmoadmin:v1.7.0