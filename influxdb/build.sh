# build image
docker build -t lirimia/influxdb .

# test image !
docker tag lirimia/influxdb lirimia/influxdb:1.8.10

# push image
docker login
docker push lirimia/influxdb
docker push lirimia/influxdb:1.8.10