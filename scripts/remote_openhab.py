import os

def get_name(remote):
    with open(remote, 'r') as i:
        return i.readline().split('=')[1].split('"')[1]

def get_sitemap_items(remote):
    with open(remote, 'r') as r:
        lines = r.readlines()

    sitemap_items = list()
    for line in lines:
        line = line.strip('\n')

        if '//' in line:
            continue

        if 'item=' in line:
            item = line.split('item=')[1].split(' ')[0]

            if item not in sitemap_items:
                sitemap_items.append(item)

    return sitemap_items

def get_influxdb_items(remote):
    with open(remote, 'r') as r:
        lines = r.readlines()

    influxdb_items = list()
    for line in lines:
        line = line.strip('\n')

        if 'Chart' in line and 'item=' in line:
            item = line.split('item=')[1].split(' ')[0]

            if item not in influxdb_items:
                influxdb_items.append(item)
            
    return influxdb_items

def get_rule_items(rules):
    with open(rules, 'r') as r:
        lines = r.readlines()

    hs = False
    rule_items = list()
    for line in lines:
        line = line.strip('\n')

        if 'when' in line:
            hs = True
            continue

        if 'then' in line:
            hs = False
            continue

        if hs is True:
            rule_items.append(line.split(' ')[5])

    return rule_items

def get_item_files():
    ''' Concatenate item files '''
    item_files = list()
    for root, dirs, files in os.walk('/home/pi/remote/items/'):
        for item_file in files:
            with open(os.path.join('/home/pi/remote/items/', item_file), 'r') as itf:
                content = itf.readlines()

            for line in content:
                line = line.strip('\n')
                item_files.append(" ".join(line.split()))

    return item_files

def compute_item_lines(remote_items, item_files, influxdb, server):
    remote_item_lines = list()
    remote_item_lines.append('%s%s "Remote@%s" ["Sensor"]' % ('Group g', server, server))

    for remote_item in remote_items:
        for line in item_files:
            if '//' in line or len(line.split(' ')) < 4:
                continue 

            type = line.split(' ')[0]
            item = line.split(' ')[1]
            item_line = type + ' ' + item

            if remote_item == item:
                label = ''
                if '"' in line:
                    label = line.split('"')[1]
                if label != '':
                    item_line += ' "' + label + '"'

                icon = ''
                if '<' in line:
                    icon = line.split('<')[1].split('>')[0]
                if icon != '':
                    item_line += ' <' + icon + '>'

                group = 'gRemote'
                if item in influxdb:
                    group = 'gInflux, gRemote'
                item_line += ' (' + group + ')'

                item_line += ' ["Status"] '

                if 'unit=' in line:
                    unit = line.split('unit="')[1].split('"')[0]
                    item_line += ' {channel="remoteopenhab:server:' + server.lower() + ':' + item + '", unit="' + unit + '"}'
                else:
                    item_line += ' {channel="remoteopenhab:server:' + server.lower() + ':' + item + '"}'

                remote_item_lines.append(item_line)

    return remote_item_lines

def genereate_remote_items(items, location):
    with open(location, 'w') as o:
        o.write('\n'.join(items))

def generate_remote_sitemap(name, remote, local):
    with open(remote, 'r') as r:
        lines = r.readlines()

    sitemap = list()
    for line in lines:
        line = line.strip('\n')

        if 'default' in line:
            sitemap.append(line.replace('default', name.lower()))
        else:
            sitemap.append(line)

    with open(local, 'w') as o:
        o.write('\n'.join(sitemap))


if __name__ == '__main__':
    input_sitemap = '/home/pi/remote/default.sitemap'
    input_rules = '/home/pi/remote/dynamic_labels.rules'

    # Remote site name
    sitemap_name = get_name(input_sitemap)

    # Remote sitemap items
    sitemap_items = get_sitemap_items(input_sitemap)

    # Items used on InfluxDB
    influxdb_items = get_influxdb_items(input_sitemap)

    # Items used on the dynamic_labels rules
    rule_items = get_rule_items(input_rules)

    # Items used on remote sitemap or rule file
    remote_items = sitemap_items.copy()
    for item in rule_items:
        if item not in remote_items:
            remote_items.append(item)

    # Concatenate all remote item files
    item_files = get_item_files()

    # generate remote items
    remote_item_lines = compute_item_lines(remote_items, item_files, influxdb_items, sitemap_name)
    genereate_remote_items(remote_item_lines, '/home/pi/remote/' + sitemap_name.lower() + '.items')

    # generate remote sitemap
    generate_remote_sitemap(sitemap_name, input_sitemap, '/home/pi/remote/' + sitemap_name.lower() + '.sitemap')
