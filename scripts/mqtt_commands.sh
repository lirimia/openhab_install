#!/bin/bash

SUDO=''
[[ $(id -u) -ne 0 ]] && SUDO='sudo'

[ $# -eq 0 ] && DEBUG=false
[ $# -eq 1 ] && [ $1 == '-debug' ] && DEBUG=true

[ $DEBUG = false ] && sleep 60

mosquitto_sub -h localhost -t 'rpi/#' -v | while read -r topic payload

do
    [ $DEBUG = true ] && echo "${topic}: ${payload}"

    COMMAND=$(echo "${topic}" | awk '{print $2}' FS='/')

    if [ $COMMAND == 'reboot' ] && [ ${payload} == 'ON' ]; then
        $SUDO reboot
    fi

    if [ $COMMAND == 'poweroff' ] && [ ${payload} == 'ON' ]; then
        $SUDO poweroff
    fi

    if [ $COMMAND == 'reset' ] && [ ${payload} == 'ON' ]; then
        # Restart docker
        cd /home/pi/IOTstack/
        docker-compose --project-directory /home/pi/IOTstack/ stop

        # Start services
        $SUDO service docker restart
        docker-compose --project-directory /home/pi/IOTstack/ up -d
        
        # Stopping on-demand containers
        echo "Stopping on-demand containers ..."
        docker stop mycontroller
        docker stop tasmoadmin
        docker stop frontail
        docker stop portainer-ce
        echo
    fi

    if [ $COMMAND == 'sync' ] && [ ${payload} == 'ON' ]; then
        /home/pi/scripts/oh.sh --remote
    fi

    if [ $COMMAND == 'backup' ] && [ ${payload} == 'ON' ]; then
        /home/pi/scripts/bkr.sh --save
    fi
done
