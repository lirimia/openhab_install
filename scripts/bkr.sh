#!/usr/bin/env bash

function backup () {
    # Check if OpenHAB config folder is commited in Git
    cd /home/pi/openhab/conf

    OH_CONF_SAVED=$(git status | grep 'working tree clean' | wc -l)
    if [ $OH_CONF_SAVED -ne 1 ]; then
        echo 'OpenHAB configuration not saved in Git. Exiting ...'
        exit
    fi

    OH_CONF_SAVED=$(git status | grep 'Your branch is ahead' | wc -l)
    if [ $OH_CONF_SAVED -eq 1 ]; then
        echo 'OpenHAB configuration not pushed to BitBucket. Exiting ...'
        exit
    fi

    # Stop services
    cd /home/pi/IOTstack/
    docker-compose --project-directory /home/pi/IOTstack/ stop
    sudo service docker restart
    echo

    # remove temporary stuff
    sudo rm -rf /home/pi/IOTstack/volumes/pihole
    sudo rm -rf /home/pi/IOTstack/volumes/openhab/userdata/backup/

    # Backup IOTstack
    echo "IOTstack backup in progress ..."
    sudo chown -R pi:pi /home/pi/IOTstack/volumes

    cd /home/pi/IOTstack/
    ./scripts/backup.sh 1

    # Stop on-demand containers
    echo "Stopping on-demand containers ..."
    docker stop mycontroller
    docker stop tasmoadmin
    docker stop frontail
    docker stop portainer-ce
    echo

    # Wait 120 seconds for the containers to start
    sleep 120

    # Save backups
    if [ ! -d /home/pi/backups/older/ ]; then
        mkdir -p /home/pi/backups/older
    fi

    if [ ! -d /home/pi/backups/latest/ ]; then
        mkdir /home/pi/backups/latest
    fi

    if [ $( ls -alh /home/pi/backups/latest/ | wc -l | awk '{print $1}') -gt 3 ]; then
        mv -f /home/pi/backups/latest/* /home/pi/backups/older/
    fi

    mv /home/pi/IOTstack/backups/backup/$(ls -trh /home/pi/IOTstack/backups/backup/ | tail -n 1) /home/pi/backups/latest

    while [ $(ls -lth /home/pi/backups/older | grep backup_ | wc -l) -gt 5 ]; do
        echo "Removing old $(ls -th /home/pi/backups/older | tail -n 1)"
        rm /home/pi/backups/older/$(ls -th /home/pi/backups/older/ | tail -n 1)
    done

    echo "Uploading latest backup on Dropbox ..."
    location=$(hostname)
    /usr/bin/rclone sync -v /home/pi/backups/latest dropbox:/$location
}

function restore () {
    # Get a fresh copy of IOTstack
    if [ -f /home/pi/IOTstack/docker-compose.yml ]; then
        cd /home/pi/IOTstack

        docker-compose --project-directory /home/pi/IOTstack/ down
        echo 'y' | docker system prune --volumes
        # echo 'y' | docker image prune -a

        cd /home/pi
    fi

    echo ''
    sudo rm -rf /home/pi/IOTstack
    git clone https://github.com/SensorsIot/IOTstack.git /home/pi/IOTstack
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/IOTstack/docker-compose.override.yml -o /home/pi/IOTstack/docker-compose.override.yml
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/IOTstack/env -o /home/pi/IOTstack/.env

    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/tasmoadmin/Dockerfile -o /home/pi/IOTstack/.templates/tasmoadmin/Dockerfile --create-dirs
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/mycontroller/Dockerfile -o /home/pi/IOTstack/.templates/mycontroller/Dockerfile --create-dirs
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/grafana/Dockerfile -o /home/pi/IOTstack/.templates/grafana/Dockerfile --create-dirs
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/influxdb/Dockerfile -o /home/pi/IOTstack/.templates/influxdb/Dockerfile --create-dirs
    curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/influxdb/create_db.iql -o /home/pi/IOTstack/.templates/influxdb/create_db.iql --create-dirs


    HOSTNAME_LOWER=$(hostname | tr '[:upper:]' '[:lower:]')
    sed -i "s|conext-me|$HOSTNAME_LOWER|" /home/pi/IOTstack/docker-compose.override.yml

    sed -i "s|YOURS|$HOSTNAME_LOWER|" /home/pi/IOTstack/duck/duck.sh
    sed -i "s|YOUR_DUCKDNS_TOKEN|5d4a21d0-8de9-4593-8a38-74ec442b7b60|" /home/pi/IOTstack/duck/duck.sh

    # Restore OpenHAB config from BitBucket
    echo ''
    sudo rm -rf /home/pi/openhab
    git clone git@bitbucket.org:lirimia/openhab_config.git --branch $(hostname) /home/pi/openhab/conf
    git -C /home/pi/openhab/conf/ remote set-url origin https://lirimia:ATBBteuf4bGWcntv5dL9eh2SbG6L4EB8D203@bitbucket.org/lirimia/openhab_config.git

    # # Sync remote sitemap
    # echo ''
    # echo 'Sync remote site data'
    # /home/pi/scripts/oh.sh --remote

    # Sync backups from Dropbox
    echo ''
    echo "Downloading latest backup from Dropbox ..."

    if [ ! -d /home/pi/backups/latest/ ]; then
        mkdir -p /home/pi/backups/latest
    fi

    location=$(hostname)
    rclone sync -v dropbox:/$location /home/pi/backups/latest

    # Restore IOTstack from latest backup
    mkdir /home/pi/IOTstack/backups
    echo "Most recent backup is: $(ls -trh /home/pi/backups/latest | tail -n 1)"
    cp /home/pi/backups/latest/$(ls -trh /home/pi/backups/latest | tail -n 1) /home/pi/IOTstack/backups/backup.tar.gz

    cd /home/pi/IOTstack
    echo 'y' | ./scripts/restore.sh
    sudo service docker restart
    docker-compose up -d --remove-orphans

    # Stop on-demand containers
    echo
    echo "Stopping on-demand containers ..."
    docker stop mycontroller
    docker stop tasmoadmin
    docker stop frontail
    docker stop portainer-ce

    if [ $(hostname) == 'pOpenHAB' ]; then
        docker stop broadlink-mqtt
    fi

    # Cleanup
    rm /home/pi/IOTstack/backups/backup.tar.gz
}

if [ ! -f /home/pi/.ssh/id_rsa ]; then
    ###############################################################################
    # Generate SSH key pair
    ###############################################################################
    echo -e "\n" | ssh-keygen > /dev/null
    eval $(ssh-agent)
    ssh-add /home/pi/.ssh/id_rsa

    echo ''
    echo "Login to BitBucket, go to Profile/Personal Settings/SSH keys"
    echo "Delete old ssh key for $(hostname) (if any)"
    echo "Add a new key having the following content:"

    echo ''
    cat /home/pi/.ssh/id_rsa.pub

    echo ''
    read -n 1 -s -r -p "Press any key to continue"
fi

if [ ! -f /home/pi/.config/rclone/rclone.conf ]; then
    ###############################################################################
    # Install rclone (for Dropbox), see https://github.com/pageauc/rclone4pi/wiki
    ###############################################################################
    curl -L https://raw.github.com/pageauc/rclone4pi/master/rclone-install.sh | bash

    echo '[dropbox]' > /home/pi/.config/rclone/rclone.conf
    echo 'type = dropbox' >> /home/pi/.config/rclone/rclone.conf
    echo 'token = {"access_token":"sO4IMZEhGJAAAAAAAAAADe49XGAFVFUxGb6Q7mcgm-TC4DMTBnZ-Xgg5x6cM5rEf","token_type":"bearer","expiry":"0001-01-01T00:00:00Z"}' >> /home/pi/.config/rclone/rclone.conf

    echo '' >> /home/pi/.config/rclone/rclone.conf

    if [ ! -d /home/pi/backups ]; then
        mkdir /home/pi/backups
    fi
fi

if test $# -eq 1; then
    case "$1" in
        --save)
            backup
            ;;
        --restore)
            restore
            ;;
        *)
            echo 'Usage: bkr.sh [--save | --restore ]'
            ;;
    esac
else
    echo 'Usage: bkr.sh [--save | --restore ]'
fi
