#!/usr/bin/env bash

DOCKER="docker exec -it influxdb influx -host localhost -username admin -password admin -database openhab_db"

function rename() {
    set -f

    IDB="select * into tmp_measurement from $1"
    $DOCKER -execute "$IDB"
    sleep 5

    IDB="drop measurement $1"
    $DOCKER -execute "$IDB"
    sleep 5

    IDB="select * into tmp_measurement from $2"
    $DOCKER -execute "$IDB"
    sleep 5

    IDB="drop measurement $2"
    $DOCKER -execute "$IDB"
    sleep 5

    IDB="select * into $2 from tmp_measurement"
    $DOCKER -execute "$IDB"
    sleep 5

    IDB="drop measurement tmp_measurement"
    $DOCKER -execute "$IDB"
    sleep 5

    set +f
}

function update() {
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get autoremove -y
    sudo apt-get autoclean -y
}

function inspect() {
    curl --silent -X GET --header "Accept: application/json" http://10.0.0.10:4050/rest/items?recursive=false | jq -r '.[] | "\(.name) \(.state)"' | grep NULL | grep -v '^g'
}

function remote() {
    # Set branch name
    if [ $(hostname) == 'pOpenHAB' ]; then
        branch='hOpenHAB'
    else
        branch='pOpenHAB'
    fi

    # Get OpenHAB config from BitBucket
    if [ -d  /home/pi/remote/ ]; then
        rm -rf /home/pi/remote/
    fi

    git clone git@bitbucket.org:lirimia/openhab_config.git --branch $branch /home/pi/remote
    cp /home/pi/remote/sitemaps/default.sitemap /home/pi/remote
    cp /home/pi/remote/rules/dynamic_labels.rules /home/pi/remote

    # Process remote data
    python /home/pi/scripts/remote_openhab.py

    # Rename rule file
    name=$(ls /home/pi/remote/ | grep -i '.items' | awk -F '.' '{print $1}')
    cp /home/pi/remote/dynamic_labels.rules /home/pi/remote/$name.rules

    sudo cp /home/pi/remote/$name.items /home/pi/openhab/conf/items
    sudo cp /home/pi/remote/$name.rules /home/pi/openhab/conf/rules
    sudo cp /home/pi/remote/$name.sitemap /home/pi/openhab/conf/sitemaps

    sudo chown openhab:openhab /home/pi/openhab/conf/items/$name.items
    sudo chown openhab:openhab /home/pi/openhab/conf/rules/$name.rules
    sudo chown openhab:openhab /home/pi/openhab/conf/sitemaps/$name.sitemap

    rm -rf /home/pi/remote/
}

if test $# -eq 1; then
    case "$1" in
        --update)
            update
            ;;
        --inspect)
            inspect
            ;;
        --remote)
            remote
            ;;
        *)
            echo 'Usage: oh.sh [--update | --remote | --inspect | --rename ]'
            ;;
    esac
elif test $# -eq 3; then
    case "$1" in
        --rename)
            rename $2 $3
            ;;
        *)
            echo 'Usage: oh.sh [--update | --remote | --inspect | --rename ]'
            ;;
    esac
else
    echo 'Usage: oh.sh [--update | --remote | --inspect | --rename ]'
fi