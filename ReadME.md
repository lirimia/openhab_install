This script automatically installs OpenHAB on top of RaspberryPi (Buster) with Frontail, InfluxDB and Grafana

# Installation
- use Raspberry PI Imager hidden menu (CTRL + SHIFT + X) to burn latest image (2022-01-28-raspios-bullseye-armhf-lite.img) with following customisations
  - hostname
  - SSH password
  - locale
- curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/setup.sh | bash | tee -a /home/pi/install.log

# Based on
- https://github.com/openhab/openhabian/blob/master/openhabian-setup.sh
- https://docs.influxdata.com/influxdb/v1.7/tools/shell/
- https://community.influxdata.com/t/no-cli-after-installing-influxdb-on-raspberry-pi/3512/6
- https://community.openhab.org/t/influxdb-grafana-persistence-and-graphing/13761
- https://community.openhab.org/t/influxdb-grafana-persistence-and-graphing/13761/86
- https://community.openhab.org/t/influxdb-grafana-installation-error/51708/13

# Move the filesystem to a USB stick/Drive
- burn the same image on the SDCard and USB
- mount USB
    sudo mkdir -p /mnt/usb_boot
    sudo mkdir -p /mnt/usb_root

    sudo mount /dev/sda1 /mnt/usb_boot
    sudo mount /dev/sda2 /mnt/usb_root
- move boot partition
    sudo rm -rf /boot/*
    sudo rsync -axv /mnt/usb_boot /boot
- update fstab on USB
    sudo blkid
    sudo nano -w /mnt/usb_root/etc/fstab                # update PARTUUID
- unmount USB
    sudo umount /mnt/usb_boot
    sudo umount /mnt/usb_root
- additional stuff
    sudo touch /boot/ssh
    echo "program_usb_timeout=1" | sudo tee -a > /boot/config.txt
    echo "max_usb_current=1" | sudo tee -a > /boot/config.txt
- verify the proper drive is used for root partition upon reboot
    sudo reboot
    findmnt -n -o SOURCE /
