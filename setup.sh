#!/bin/bash

VERSION=$(grep 'VERSION_CODENAME' /etc/os-release | awk -F '=' '{print $2}')

echo
echo "**************************************************"
echo "* Set locale                                     *"
echo "**************************************************"
sudo sed -i "s|# en_GB.UTF-8 UTF-8|en_GB.UTF-8 UTF-8|" /etc/locale.gen
sudo sed -i "s|# en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|" /etc/locale.gen
sudo locale-gen

echo '' | sudo tee -a /home/pi/.bashrc
echo 'export LC_ALL="en_US.UTF-8"' | sudo tee -a /home/pi/.bashrc

echo '' | sudo tee -a /home/pi/.bashrc
echo alias ll=\'ls -alh\' | sudo tee -a /home/pi/.bashrc
echo alias karaf=\"sshpass -p 'habopen' ssh openhab@localhost -p 8101\" | sudo tee -a /home/pi/.bashrc


echo
echo "**************************************************"
echo "* Updating linux                                 *"
echo "**************************************************"
sudo apt-get update
sudo apt-get upgrade -y


echo
echo "**************************************************"
echo "* Install required packages                      *"
echo "**************************************************"
sudo apt-get install -y snmp snmpd
sudo apt-get install -y sshpass
sudo apt-get install -y dos2unix
sudo apt-get install -y jq
sudo apt-get install -y pv
sudo apt-get install -y mc
sudo apt-get install -y zip
sudo apt-get install -y unrar-free
sudo apt-get install -y mosquitto-clients
sudo apt-get install -y git
sudo apt-get install -y moreutils
sudo apt-get install -y bluetooth 

git config --global user.name 'Laur'
git config --global user.email 'lirimia@gmail.com'


echo
echo "**************************************************"
echo "* Get scripts                                    *"
echo "**************************************************"
curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/scripts/bkr.sh --create-dirs -o /home/pi/scripts/bkr.sh
curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/scripts/oh.sh --create-dirs -o /home/pi/scripts/oh.sh
curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/scripts/mqtt_commands.sh --create-dirs -o /home/pi/scripts/mqtt_commands.sh
curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/scripts/remote_openhab.py --create-dirs -o /home/pi/scripts/remote_openhab.py


chmod +x /home/pi/scripts/*

echo '' | sudo tee -a /home/pi/.bashrc
echo 'export PATH=$PATH:/home/pi/scripts' | sudo tee -a /home/pi/.bashrc

echo '0 0 * * * /home/pi/scripts/oh.sh --update' | sudo tee -a /var/spool/cron/pi
echo '@reboot /home/pi/scripts/mqtt_commands.sh &' | sudo tee -a /var/spool/cron/pi


echo
echo "**************************************************"
echo "* Docker                                         *"
echo "**************************************************"
export LC_ALL="en_GB.UTF-8"

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker pi
sudo usermod -aG bluetooth pi
sudo chmod 666 /var/run/docker.sock
rm get-docker.sh


echo
echo "**************************************************"
echo "* IOTstack & DuckDNS                             *"
echo "**************************************************"
sudo apt install -y python3-pip python3-dev docker-compose
sudo pip3 install -U ruamel.yaml==0.16.12 blessed

git clone https://github.com/SensorsIot/IOTstack.git /home/pi/IOTstack
curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/IOTstack/env -o /home/pi/IOTstack/.env


HOSTNAME_LOWER=$(hostname | tr '[:upper:]' '[:lower:]')

sed -i "s|YOURS|$HOSTNAME_LOWER|" /home/pi/IOTstack/duck/duck.sh
sed -i "s|YOUR_DUCKDNS_TOKEN|5d4a21d0-8de9-4593-8a38-74ec442b7b60|" /home/pi/IOTstack/duck/duck.sh

echo '*/5 * * * * sudo /home/pi/IOTstack/duck/duck.sh > /dev/null 2>&1' | sudo tee -a /var/spool/cron/pi


echo
echo "**************************************************"
echo "* SpeedTest w/ OpenHAB icons                     *"
echo "**************************************************"
wget http://http.us.debian.org/debian/pool/main/libs/libseccomp/libseccomp2_2.5.4-1+b3_armhf.deb
sudo dpkg -i libseccomp2_2.5.4-1+b3_armhf.deb


echo
echo "**************************************************"
echo "* Create additional users                        *"
echo "**************************************************"
sudo groupadd -g 9001 openhab
sudo useradd -u 9001 -g openhab -c 'OpenHAB configuration account' -s /bin/bash -d /home/openhab -m -r openhab
echo -e "openhab\nopenhab" | sudo passwd openhab

echo '' | sudo tee -a /home/openhab/.bashrc
echo 'export LC_ALL="en_US.UTF-8"' | sudo tee -a /home/openhab/.bashrc

echo '' | sudo tee -a /home/openhab/.bashrc
echo alias ll=\'ls -alh\' | sudo tee -a /home/openhab/.bashrc
echo alias karaf=\"sshpass -p 'habopen' ssh openhab@localhost -p 8101\" | sudo tee -a /home/openhab/.bashrc


echo
echo "**************************************************"
echo "* Static IP                                      *"
echo "**************************************************"
if [ $VERSION == 'bullseye' ]; then
    echo "" | sudo tee -a /etc/dhcpcd.conf
    echo "# Static IP Configuration" | sudo tee -a /etc/dhcpcd.conf
    echo "interface eth0" | sudo tee -a /etc/dhcpcd.conf
    echo "static ip_address=10.0.0.10/24" | sudo tee -a /etc/dhcpcd.conf
    echo "static routers=10.0.0.1" | sudo tee -a /etc/dhcpcd.conf
    echo "static domain_name_servers=10.0.0.1 193.230.161.3 8.8.8.8" | sudo tee -a /etc/dhcpcd.conf
fi

if [ $VERSION == 'bookworm' ]; then
    sudo nmcli connection modify 'Wired connection 1' ipv4.addresses 10.0.0.10/24 ipv4.method manual
    sudo nmcli connection modify 'Wired connection 1' ipv4.gateway 10.0.0.1
    sudo nmcli connection modify 'Wired connection 1' ipv4.dns "10.0.0.1 193.230.161.3 8.8.8.8"
fi

echo "10.0.0.10       $(hostname)" | sudo tee -a /etc/hosts


echo
echo "**************************************************"
echo "* Cleaning linux                                 *"
echo "**************************************************"
/usr/bin/crontab /var/spool/cron/pi

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y


echo
echo "**************************************************"
echo "* Append script VersionID to log file            *"
echo "**************************************************"
git clone  --quiet https://bitbucket.org/lirimia/openhab_install.git
cd openhab_install/
version=$(git log --oneline | head -1 | awk '{print $1}')
cd ..
rm -rf openhab_install

echo "Logfile for OpenHAB Install script version $version"
echo "Installation completed!"

sudo reboot
