# compile latest MySensors gateway
git clone https://github.com/mysensors/MySensors.git --branch master mysensors

if [ $(grep 'VERSION_CODENAME' /etc/os-release | awk -F '=' '{print $2}') == 'bullseye' ]; then
    # https://forum.mysensors.org/topic/11138/mysensors-build-problem-on-ubuntu-20-04-rpi-or-tinker-board/3
    sudo touch /usr/include/stropts.h

    # https://github.com/mysensors/MySensors/pull/1454
    curl -L https://raw.githubusercontent.com/functionpointer/MySensors/b6f154bfd43e0da0f3f8c9752068f96ed7e86c16/hal/architecture/Linux/drivers/core/config.c -o mysensors/hal/architecture/Linux/drivers/core/config.c
    curl -L https://raw.githubusercontent.com/functionpointer/MySensors/b6f154bfd43e0da0f3f8c9752068f96ed7e86c16/hal/architecture/Linux/drivers/core/config.h -o mysensors/hal/architecture/Linux/drivers/core/config.h
fi

./mysensors/configure --my-transport=rf24 --my-rf24-irq-pin=15 --my-rf24-channel=107 --my-gateway=mqtt --my-controller-ip-address=10.0.0.10 --my-mqtt-publish-topic-prefix=mysensors/stat --my-mqtt-subscribe-topic-prefix=mysensors/cmnd --my-mqtt-client-id=mysgw
mv ./Makefile.inc mysensors
make -C mysensors

# build image
docker build -t lirimia/mysgw .

# test image !
docker tag lirimia/mysgw lirimia/mysgw:v2.3.2

# push image
docker login
docker push lirimia/mysgw
docker push lirimia/mysgw:v2.3.2