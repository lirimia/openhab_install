# build image
docker build -t lirimia/grafana .

# test image !
docker tag lirimia/grafana lirimia/grafana:v8.4.4

# push image
docker login
docker push lirimia/grafana
docker push lirimia/grafana:v8.4.4